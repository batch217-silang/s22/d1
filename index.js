console.log("Hello Philippines");
// [SECTION] ARRAY METHODS
// manipulate data within an array

// 1. MUTATOR METHODS
let fruits = ["Apple", "Orange", "Kiwi", "Dragon fruit"];

// push() - ADDING an element 
// syntax arrayName.push();

console.log("Current array");
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits);


// ADDING MULTIPLE ELEMENTS
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method');
console.log(fruits);

// pop () - REMOVE/DELETE 
// syntax arrayName.pop();
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:')
console.log(fruits);
fruits.pop();
console.log('Mutated array from pop method:')
console.log(fruits);

// 2. UNSHIFT METHOD adds one or more at the BEGINNING 
// syntax: arrayName.unshift('elementA'...);
fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

// shift () - removes an element at the beginning of an array
// syntax: arrayName.shift();
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift methos:");
console.log(fruits);

// splice() - removes and adds 
// syntax: arrayNAme.splice(startingIndex, deleteCount, elements to be added)
fruits.splice(1, 2, 'Lime', 'Cherry')
console.log('Mutated array from splice method');
console.log(fruits);

// sort() - rearranges the array elements in alphanumeric order

fruits.sort();
console.log('Mutated array from sort method');
console.log(fruits);

// reverse() - reverses the order 
fruits.reverse();
console.log('Mutated array from reverse method');
console.log(fruits);


// NON MUTATOR METHODS - do not modify/change an array

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
// indexOf() - return the index number if there's a match
// syntax -- arrayName.indexOf(SearchValue);
// syntax -- arrayName.indexOf(SearchValue, fromIndex);
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Resule of indexOf method: ' + invalidCountry);


// lastIndexOf()
// syntax: arrayName.lastIndexOf(searchValue)

// getting index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method ' + lastIndex);

// getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 8);
console.log('Result of lasIndexOf method ' + lastIndexStart);

// slice()
// syntax: arrayName.slice(startingIndex);
// slicing off elements from a specified index
let sliceArrayA = countries.slice(2);
console.log('Result from slice method:');
console.log(sliceArrayA);



let sliceArrayB = countries.slice(2, 4);
console.log('Result from slice method:');
console.log(sliceArrayB);


let sliceArrayC = countries.slice(-3);
console.log('Result from slice method:');
console.log(sliceArrayC);

// toString()
let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);


// concat()
let tasksArrayA = ['drink HTML', 'eatjavascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];
let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method');
console.log(tasks);

// combining multiple arrays
console.log('result from concat method');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining outside element
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('result from concat method');
console.log(combinedTasks);

// join()
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log (users.join());
console.log (users.join(''));
console.log (users.join(' - '));
console.log (users.join(' ! '));


// ITERATION METHOD
// forEach() - similar to for loops
// arrayName.forEach(function(indivElement){statement})
allTasks.forEach(function(tasks){
	console.log(tasks);
});

// forEach() with conditional statements
let fliteredTasks = [];
allTasks.forEach(function(tasks){
	if(tasks.length > 10){
		fliteredTasks.push(tasks);
	}
});
console.log('Result of fliteredTasks:');
console.log(fliteredTasks);

// map() 
// syntax let/const resultArray = arrayName.map(function(indivElement))
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(numbers){
	return numbers * numbers;
});
console.log('Original Array:');
console.log(numbers);
console.log('Result of map method');
console.log(numberMap);


// map vs. foreach
let numberForEach = numbers.forEach(function(numbers){
	return numbers * numbers;
});
console.log(numberForEach);


// every() -checks if ALL ELEMENTS SHOULD the given condition. For validating data stored
let allValid = numbers.every(function(numbers){
	return (numbers > 3)
});
console.log('Result of every method');
console.log(allValid);


// some() - at least 1 
let someValid = numbers.some(function(numbers){
	return (numbers < 2)
});
console.log('Result of some method');
console.log(someValid);


if(someValid){
	console.log('Some numbers in the array are greater than 2');
};

// filter()
/*// syntax - let/const resultArray = arrayNAme.filter(function(indivElement){
	return expression/condition
});*/

let filterValid = numbers.filter(function(numbers){
	return (numbers < 4);
});
console.log('result from filter method');
console.log(filterValid);


// no element found
let nothingFound = numbers.filter(function(numbers){
	return (number = 0);

});
console.log('Result of filter method');
console.log(nothingFound);

// filtering using forEach
let filteredNumbers = [];
numbers.forEach(function(numbers){
	if(numbers < 3){
		filteredNumbers.push(numbers);
	}
});
console.log('Result of filter method');
console.log(filteredNumbers);

// includes() - checks the arguments, bolean
// syntax -- arrayName.includes(<argument>)

let products = ['Mouse', "Keyboard", "Laptop", "Monitor"];
let productFound1 = products.includes("Mouse");
console.log(productFound1);
let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce
/*
syntax: let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
	return expression/condition
})
*/

let iteration = 0;
let reduceArray = numbers.reduce(function(x, y){
	console.warn("current iteration" + ++iteration);
	console.log("accumulator: " + x); 
	console.log("currentValue: " + y); 

	return x + y;
});
console.log('Result of reduced method: ' + reduceArray);